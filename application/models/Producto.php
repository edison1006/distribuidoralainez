<?php

class Producto extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }
  //funcion para insertar un instructor
  function insertar($datos){
    //CONSULTAR ACTIVE RECORD -> CodeIgniter Inyeccion SQL
    return $this->db->insert("producto",$datos);
  }
  //Funcion para consultar productos
  function obtenerTodos(){
    $listadoProductos=$this->db->get("producto");
    if($listadoProductos->num_rows()>0){//Si hay datos
    return $listadoProductos->result();
      }else{
        return false;
      }
  }
  //borrar instructores
  function borrar($id_pro)
  {
    $this->db->where("id_pro",$id_pro);
    return $this->db->delete("producto");
  }

}//cierre de la clase

?>
