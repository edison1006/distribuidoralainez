<div id="ubicanos"class="row text-center">
  <h1 class="text-center" >UBÍCANOS</h1>
  <h3>Nos encontramos ubicados en la calles Joaquin Enriquez y Palemon Monroy</h3>
  <div id="mapaDireccion" style="width:100%; height:300px;"></div>
  <script type="text/javascript">
         function initMap(){
             // creando el punto central del mapa
             var coordenadaCentral=new google.maps.LatLng(-0.3113302525812672, -78.56306055187045);
             //Creando el mapa
             var mapa1=new google.maps.Map(document.getElementById('mapaDireccion'),
             {
                 center:coordenadaCentral,
                 zoom:15,
                 mapTypeId:google.maps.MapTypeId.ROADMAP
             }
             );
         }
     </script>
     <br><br>
</div>
