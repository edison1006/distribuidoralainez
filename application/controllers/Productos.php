<?php
  class Productos extends CI_Controller
  {

    function __construct()
    {
      parent:: __construct();
      //cargar modelo
      $this->load->model('Producto');
    }
    //renderiza la vista listado
    public function listarproducto(){
      $data['productos']=$this->Producto->obtenerTodos();
      $this->load->view('header');
      $this->load->view('productos/listarproducto',$data);
      $this->load->view('footer');
    }
    public function nuevoproducto(){
      $this->load->view('header');
      $this->load->view('productos/nuevoproducto');
      $this->load->view('footer');
    }
    public function guardar(){
    //codigo neto
    $datosNuevoProducto=array(
      "nombre_pro"=>$this->input->post('nombre_pro'),
      "descripcion_pro"=>$this->input->post('descripcion_pro'),
      "precio_doce_pro"=>$this->input->post('precio_doce_pro'),
      "precio_uni_pro"=>$this->input->post('precio_uni_pro'),
      "cantidad_pro"=>$this->input->post('cantidad_pro')
    );
    if ($this->Producto->insertar($datosNuevoProducto)) {
      redirect('productos/listarproducto');
    }else {
        echo "<h1>ERROR AL INSERTAR</h1>";
      }
    }
    //funcion para eliminar instructores
    public function eliminar($id_pro){
      if ($this->Producto->borrar($id_pro))
      {//invocando el modelo
        redirect('productos/listarproducto');
      }else{
        echo "ERROR AL BORRAR :()";
      }
    }
  }
?>
