<div class="container">
  <div class="row">
    <div class="col-md-6 text-center">
      <h2><b>MISIÓN</b><h2>
        <img src="<?php echo base_url() ?>/assets/images/vision.jpg" height="300px" alt="">
        <br><br>
        <p class="text-justify">Ofrecer a nuestros clientes una amplia gama de sandalias de alta calidad, que combinen confort y estilo, y que sean amigables con el medio ambiente. Nos comprometemos a brindar un servicio excepcional al cliente, promover un ambiente de trabajo positivo y colaborativo para nuestro equipo, y apoyar causas sociales y ambientales para hacer del mundo un lugar mejor.</p>
    </div>
    <div class="col-md-6 text-center">
      <h2><b>VISIÓN</b><h2>
        <img src="<?php echo base_url() ?>/assets/images/banner1.png" height="300px" alt="">
          <br><br>
          <p class="text-justify">Ser la marca líder en el mercado de sandalias, reconocida por la calidad, comodidad y estilo de nuestros productos, y por nuestra contribución a la protección del medio ambiente.</p>
    </div>
  </div>
</div>
